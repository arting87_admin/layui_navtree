# layui navTree 动态渲染多层级菜单组件演示用后台布局系统

#### 介绍
extends/navTree.js 是一个基于 layui 扩展的模块化组件，使用它您可以很方便地构建后台布局系统中的垂直导航菜单与水平导航菜单，理论上可渲染无线层级，受容器看度限制，推荐使用三级菜单。

extends/navTree.js 原本是作者在开源面板项目 Layui Fun CMS 中编写的一个基于 layui 的动态渲染菜单的组件，现在把这个扩展组件独立开发出来，使它不必依赖与 Layui Fun CMS 一起使用，只要你的后台系统符合 layui 后台布局的规范，即可独立使用这个组件，这将极大地方便开发者编写自己喜爱的代码。

extends/navTree.js 组件是用来渲染生成符合 layui 语义化的水平导航菜单与垂直导航菜单结构，导航菜单点击后将会在客户端本地存储（window.localStorage），因此刷新页面后，右侧 iframe 载入时将保持打开上一个页面链接。

看到这，您应该知道，navTree 组件适合 iframe 版的后台布局系统使用。

#### 软件架构

扩展组件：extends/navTree.js

演示地址：[https://kexin_front_end.gitee.io/layui_navtree/admin](https://kexin_front_end.gitee.io/layui_navtree/admin)

在线文档：[https://kexin_front_end.gitee.io/layui_navtree/manuals](https://kexin_front_end.gitee.io/layui_navtree/manuals)

#### 安装教程

1. 本项目是一个前端项目，但是因为在脚本方法中使用了获取 json 数据请求的方式来模拟与服务端交互，为防止浏览器提示跨域问题，因此项目文件需要部署在 web 容器下运行方可正常浏览，如：nginx、IIS、phpStudy、apache tomcat，推荐：nginx；
2. nginx.conf 相关配置：

```
#host layui_fun_cms
server {
	listen       8000;
	server_name  localhost;
	root         D:/Workspace/layui_navtree;
	client_max_body_size 200m;  

	#charset koi8-r;

	#access_log  logs/host.access.log  main;

        #转发 web api 路由	
	location /app/ {
			root   html;    
			index  index.html index.htm;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header Host $http_host;
			proxy_set_header X-NginX-Proxy true;			
			proxy_pass http://0.0.0.0:8080/;
			proxy_redirect off;
			client_max_body_size 200m;	#最大接受200m的文件
		}
}
```

3. 导航菜单有图标样式，可以使用 layui-icon 或者 font-awesome。如果您在浏览页面时图标未能正常显示，请在您的 web 服务容器中配置添加相关字体图标的 mime 类型。

#### 使用说明

1.  仅出现侧边垂直导航

```
//渲染导航菜单（仅出现侧边垂直导航）
navTree.render({
	elem: '#sideNav',	//注意框架布局中应存在你指定的垂直导航容器		
	headerNavElem: false,	//不渲染头部水平导航
	url: "api/nav", //获取菜单数据的请求地址
	recursion: true,    //指定菜单数据是否需要递归处理：true表示数据源需要递归处理；false表示数据源已是树形结构，不需要递归处理
	rootValue: "0",    //根节点菜单的主键值
	home: "page/home.html",	//默认或缺省时打开的页面链接
	currentNavKey: "current_nav",
	done: function(){
		//菜单渲染完成后的回调方法
		console.log("后台菜单渲染完成！");
	}
});
```

![渲染导航菜单（仅出现侧边垂直导航）](https://images.gitee.com/uploads/images/2019/1017/112257_e0d21262_418889.jpeg "QQ截图20191016223151.jpg")
图：渲染导航菜单（仅出现侧边垂直导航）。

2. 出现头部水平导航与侧边垂直导航（将联动）

```
//渲染导航菜单（出现头部水平导航+侧边垂直导航，将联动）
navTree.render({
	elem: '#sideNav',	//注意框架布局中应存在你指定的垂直导航容器		
	headerNavElem: '#headerNav',	//出现头部水平导航，注意布局中应存在你指定的头部水平导航容器
	url: 'api/nav', //获取菜单数据的请求地址
	recursion: true,    //指定菜单数据是否需要递归处理：true表示数据源需要递归处理；false表示数据源已是树形结构，不需要递归处理
	rootValue: "0",    //根节点菜单的主键值
	home: "page/home.html",	//默认或缺省时打开的页面链接
	currentNavKey: "current_nav",
	done: function(){
		//菜单渲染完成后的回调方法
		console.log("后台菜单渲染完成！");
	}
});
```

![渲染导航菜单（出现头部水平导航+侧边垂直导航）](https://images.gitee.com/uploads/images/2019/1017/112326_67965d9b_418889.jpeg "QQ截图20191016223346.jpg")
图：渲染导航菜单（出现头部水平导航+侧边垂直导航，将联动）。

3.  完整的参数配置


```
this.config = {
	elem: '.layui-nav[lay-filter="sideNav"]',	//选取左侧垂直导航菜单容器
	headerNavElem: '.layui-nav[lay-filter="headerNav"]',	//选取头部水平导航条容器（如果设置了此值，将会渲染显示头部水平导航条；不设置不会渲染显示）
	url: null,  //异步请求菜单数据的接口地址，
	headers: null,	//异步接口的请求头，如：headers: {token: 'secret'}
	method: "get",	//异步请求类型，默认：get	
	data: null,	//菜单数据的原生集合（Array），如果直接传入了该配置参数，url 等异步参数将会被忽略
	home: "page/home.html",	//指定一个默认或缺省时打开的页面链接，如：后台首页，防止菜单链接跳转出错
	target: "right",	//指定菜单链接打开窗口的方式
	recursion: false,	//指定菜单数据是否需要递归处理，如果接口返回的菜单数据已经是递归数据（条目属性含有 children 属性），设为 false；当设为 true 时组件将会在前端配合 props 来转换为树形结构
	rootValue: null,	//规定顶级节点的主键值，该参数用于筛选出顶级节点，进而封装多层级的导航菜单结构
	parseData: function(res){	//数据预解析函数：res 即为原始返回的数据
		return {"code": res.code, "data": res.data, "msg": res.msg};	//注意：必须至少同时返回 {"code": xxx, "data": xxx}
	},
	response: {//按需指定，目前仅需指定成功状态码。如果开发者需要配置返回结果属性，请使用 parseData 函数来转换更加实用
		"statusCode": 0,	//规定成功的状态码，默认：0
	},
	props: {	//配置菜单集合数据中各节点的属性名称
		"idKey": "id", //菜单项的主键名称
		"pidKey": "parentId", 	//父级节点属性名称
		"childrenKey": "children", //子节点导航属性名称
		"titleKey": "title", //菜单项标题属性名称（菜单项的显示文字）
		"codeKey": "code", //菜单项编码属性名称，将映射为元素上的 data-code 属性（code 需唯一）
		"hrefKey": "href", //菜单项的链接地址属性名称
		"targetKey": "target", //菜单项超链接打开窗口方式的属性名称（非必须配置）
		"iconKey": "icon", 	//菜单项的图标样式属性名称（非必须配置）
	},
	paddingLeft: "16",	//单位：px，按照层级深度设置菜单元素的左边留白，让内容缩进
	currentDataKey: "current_nav_data",	//是否缓存菜单数据在本地存储中，已提供了一个默认的缓存项的键名称，可自定义；如果不需要缓存，请设置为 false
	currentNavKey: "current_nav",	//是否缓存最近打开的导航菜单项，默认的缓存项的键名称，可自定义；如果不需要缓存，请设置为 false
	done: null	//function(res){}，动态菜单渲染完成后的回调函数
}
```

4. 基础参数一览表

![基础参数一览表](https://images.gitee.com/uploads/images/2021/0713/115811_1671210f_418889.png "基础参数一览表.png")

#### 参与贡献

1.  如发现 bug 请及时提交 issue 或在群聊中讨论，等待作者补充升级。
2.  欢迎加入本项目的官方QQ群聊：[708659082](https://jq.qq.com/?_wv=1027&k=o0GHZEuE)（Layui 前端精准问答）。
3.  本项目更新与升级将及时发布在码云仓储，请到码云上来更新最新版本。

### 更新日志

 **v1.0.2，2019-11-23** 

- 【新增】parseData 预解析函数。详细使用说明请参考组件的在线文档。
- 【新增】response 配置返回结果属性参数。详细使用说明请参考组件的在线文档。


v1.0.1，2019-10-15

- 首个版本发布，可渲染基于 layui 的水平导航与垂直导航，适用于 iframe 版的后台布局系统。
- 孵化。


#### 捐赠作者

1.  本项目由作者开发完成，原创不易，请勿翻版。“随风潜入夜，润物细无声”，感谢您的赞赏，让我今天增辉不少！
