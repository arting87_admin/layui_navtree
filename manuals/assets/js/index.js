//视图全局变量
var com = {
		startYear: 2018,	//系统上线年份
		principalName: "科新前端",	//主体名称
		basePath: "../manuals" //目录基地址
	}
	,$ = window.$ || {};    //兼容页面上单独引用的 jQuery 对象

//layui 模块引用
layui.use(['jquery', 'layer', 'util', 'laytpl'], function () {
    var layer = layui.layer, util = layui.util, laytpl = layui.laytpl;
		
    $ = layui.$;    //使用 layui 内部的 jQuery 组件
	
    //获取当前地址中的参数
    var nameHash = window.location.hash;
	
    //侧边栏目录大纲容器
    var aside = $("#asideTree");

    //构造侧边栏树形菜单 html 结构方法（isRoot：是否从根节点开始渲染）
    var renderAsideTree = function (data, isRoot) {
        if (arguments.length < 2) {
            isRoot = true;    //如未指明 isRoot，默认是从根节点开始渲染
        }
        var html = [];  // html 字串临时存储变量
        if (data && data.length > 0) {
            for (var i = 0, item; i < data.length; i++) {
                item = data[i];
                if (isRoot) {
                    html.push('<li><h2>');
                    html.push(item.title);
                    html.push('</h2></li>');
                } else {
                    html.push('<li class="aside-tree-node"><a href="');
                    html.push(item.href ? item.href : com.basePath + "/index.html#" + item.name);   //输出超链接的链接地址
                    html.push('" title="');
                    html.push(item.title);  //输出条目的标题
                    html.push(item.keywords ? '（' + item.keywords + '）' : '');    //输出标签或关键词
                    html.push('"><cite>');
                    html.push(item.title);
                    html.push('</cite>');
                    if (item.keywords) {
                        var kwArr = item.keywords.split(",");   //分隔标签或关键词字串，返回一个数组
                        if (kwArr.length > 0) {
                            html.push("<em>");
                            html.push(kwArr.join("/")); //使用 "/" 符号来连接每个标签或关键词
                            html.push("</em>");
                        }
                    }
                    html.push('</a></li>');
                    if (item.seperate) {
                        html.push('<li class="aside-tree-seperate"><hr/></li>');    //追加一个分隔符元素
                    }
                }
                if (item.children && item.children.length > 0) {
                    html.push(renderAsideTree(item.children, false));    //递归调用：构造子节点菜单的 html
                }
            }
        }
        return html.join("");
    };

    //获取侧边栏菜单数据
    $.getJSON(com.basePath + "/assets/json/aside.json", function (res) {
        var treeHtml = renderAsideTree(res); //执行构造侧边栏菜单 html 的方法
        aside.children(".layui-tree").html(treeHtml); //替换元素的 innerHTML

        //根据 namHash 地址栏参数来激活侧边栏中的菜单项
        if(nameHash){
            asideLinks = aside.children(".layui-tree").find("li>a").filter(function(index){
                return $(this).attr("href").indexOf(nameHash) > -1; //过滤表达式
            }).parent().addClass("layui-this"); //添加激活样式
        }
    });

    //章节的模板容器与渲染模板方法
    var chapterView = document.getElementById("chapterView"), chapterTpl = document.getElementById("chapterTpl");
    var renderChapterTpl = function (data) {
        laytpl(chapterTpl.innerHTML).render(data, function (render) {
            chapterView.innerHTML = render; //渲染完成，替换模板容器的 innerHTML
        });
    };

    //获取章节内容方法
    var getChapterData = function (chapterName) {
        chapterName = chapterName.replace("#", "") || "extends_navTree";

        //请求服务端数据（异步） 
        var url = com.basePath + "/assets/json/chapterJson/" + chapterName + ".json?ts=" + new Date().getTime();
        $.getJSON(url, function (res) {
            var data = res || {};
            renderChapterTpl(data);
        }).error(function () {
            renderChapterTpl({});
        });
    };

    //页面第一次加载时调用获取章节内容方法
    getChapterData(nameHash);

    //侧边导航菜单点击事件注册
    aside.on("click", ".layui-tree a", function () {
        chapterView.innerHTML = '<div class="loading-content">正在加载…</div>';
        var self = $(this), href = self.attr("href") || "", nameParam = "index";
        var hrefArr = href.split('#');
        if (hrefArr.length > 0) {
            nameParam = hrefArr[hrefArr.length - 1];
        }
        getChapterData(nameParam);  //获取获取章节内容
        $("#asideTree>.layui-tree li").removeClass("layui-this");
        self.parent("li").addClass("layui-this");
		//$(window).scrollTop(0);
    });

    //章节内容中的图片点击事件注册
	var winWidth = $(window).width();
    $("#chapterView").on("click", "img", function() {
        var self = $(this), width = winWidth*0.833333;
        layer.open({
            type: 1,
            title: false,
            closeBtn: 1,
            shadeClose: true,
            area: width + 'px',
            content: '<div class="text-center"><img alt="" style="max-width: 100%;" src="' + self.attr("src") + '"/></div>'
        });
    });

    //右下角固定块
    util.fixbar({
        bar1: false
    });

    //定义一个窗体滚动函数
    var offsetTop = 0, bottom = 0;
    var scrollFun = function(){
        offsetTop = $(window).scrollTop();
        bottom = $("#footer").offset().top - $(window).height();
        if(offsetTop > 60 && bottom > 0){
            aside.addClass("aside-fixed");
        }else {
            aside.removeClass("aside-fixed");
        }
    };

    //页面第一次加载时判断滚动条垂直位置，执行窗体滚动函数
    scrollFun();

    //监听窗体滚动事件
    $(window).on("scroll", function(){
        scrollFun();    //回调处理：执行窗体滚动函数
    })

    //footer 节点中的开始年份、当前年份与版权主体信息
	com.thisYear = new Date().getFullYear();	//当前年份
    $(".startYear").text(com.startYear); //开始年份
    $(".thisYear").text(com.thisYear); //当前年份
    $(".principalName").text(com.principalName); //当前主体
});